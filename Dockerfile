FROM redis:5-alpine

RUN apk --update --no-cache add python3 curl
RUN cd /tmp &&\
  curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py &&\
  python3 get-pip.py &&\
  rm get-pip.py

RUN pip install j2cli redis timeout-decorator requests

COPY redis.conf.j2 /usr/local/etc/redis/redis.conf.j2

ARG REDIS_PORT=6379
ENV REDIS_PORT ${REDIS_PORT}

ARG REDIS_PASS_FILE=/run/secrets/redis_pass
ENV REDIS_PASS_FILE ${REDIS_PASS_FILE}

EXPOSE ${REDIS_PORT}
EXPOSE 1${REDIS_PORT}

COPY bin/ /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]
CMD [ "redis-server", "/usr/local/etc/redis/redis.conf" ]