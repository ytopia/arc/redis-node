#!/bin/sh

set -e

if [ "$1" = 'redis-server' ]; then
  create-redis-conf
  sync-nodes-conf
  export REDISCLI_AUTH=$(cat "$REDIS_PASS_FILE")
fi

exec "$@"